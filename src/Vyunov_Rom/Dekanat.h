#include <string>
#include <iostream>
#include <fstream>
#include <ctime>

using namespace std;

class Dekanat
{
private:
	Student ** Students;
	Group ** gr;
	int StudCount;
	int GroupCount;
public:
	Dekanat()
	{
		StudCount = 0;
		GroupCount = 0;
		Students = new Student * [StudCount];
		gr = new Group * [GroupCount];
	}
	void createStudList(string Path);
	void createGroupList(string Path);
	void createStud(int ID, string FIO, string GroupTitle);
	void createGroup(string Title);
	void excludeStud(Student * st);
	void choiseHead(Group * gr);
	void choiseHeadForAll();
	void addRNDMarkForAllStud();
	void transferStud(Student * Stud, Group * toGroup);
	void checkProgress();
	void AvarageGroupRating();
	void AvarageStudRating();
	Group * findGroup(string Title);
	Student * findStud(int ID);
	Student * rndStud();
	Group * rndGroup();
	Student * findStud(string FIO);
	
	void PrintGroups()
	{
		cout << endl << "~~~~~~~~~~~~~~~~~������ �����~~~~~~~~~~~~~~~~~~~~~~" << endl<<endl;
		for (int i = 0; i < GroupCount; i++)
		{
			cout << gr[i]->getTitle() << endl;
		}
		cout << endl << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
	}
	void PrintStud()
	{
		cout << endl << "~~~~~~~~~~~~~~~������ ���������~~~~~~~~~~~~~~~~~~~~" << endl<<endl;
		for (int i = 0; i < StudCount; i++)
		{
			cout << Students[i]->getID() << "\t" << Students[i]->getFIO() << "\t" << 
				Students[i]->getGroup()->getTitle() << endl;
		}
		cout << endl << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
	}
	~Dekanat()
	{
		for (int i = 0; i < StudCount; i++)
			delete Students[i];
		delete[] Students;
		for (int i = 0; i < GroupCount; i++)
			delete gr[i];
		delete[] gr;
	}
};