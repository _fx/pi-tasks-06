#include <iostream>
#include <string>

using namespace std;

#include "Student.h"
#include "Group.h"

Group::Group() {
	title = "-";
	st = 0;
	number = 0;
	head = 0;
}

Group::Group(string title) {
	this->title = title;
	st = 0;
	number = 0;
	head = 0;
}

Group::~Group() {
	delete[] st;
	delete[] head;
}

void Group::addStudent(Student *s) {
	if ( number == 0 )
	{
		st = new Student *[1];
		st[number++] = s;
	}
	else
	{
		Student **tmp = new Student *[number+1];
		for ( int i = 0; i < number; i++ )
			tmp[i] = st[i];
		delete[] st;
		st = tmp;
		st[number++] = s;
	}
	s->toGroup(this);
}

bool Group::setRandHead() {
	if ( number != 0 )
	{
		head = st[rand()%number];
		return true;
	}
	return false;
}

bool Group::searchStudent(string fio) {
	for ( int i = 0; i < number; i++ )
		if ( st[i]->getFio() == fio )
			return true;
	return false;
}

bool Group::searchStudent(int id) {
	for ( int i = 0; i < number; i++ )
		if ( st[i]->getId() == id )
			return true;
	return false;
}

float Group::averageGrMark() {
	float sum = 0;
	if ( number != 0 )
	{
		for ( int i = 0; i < number; i++ )
			sum += st[i]->averageMark();
		return sum / (float)number;
	}
	return sum;
}

bool Group::delStudent(int id) {
	for ( int i = 0; i < number; i++ )
		if ( st[i]->getId() == id )
		{
			if (head = st[i]) head = 0;
			st[i] = st[number-- - 1];
			return true;
		}
	return false;
}

void Group::info() {
	cout << "Title: " << title << endl;
	cout << "Average mark: " << averageGrMark() << endl;
	if (head != 0 ) cout << "Head: " << head->getFio() << endl;
	cout << number << " students: " << endl;
	for ( int i = 0; i < number; i++ )
		cout << "ID: " << st[i]->getId() << " Fio: " << st[i]->getFio() << endl;
	cout << endl;
}

string Group::getTitle() const { return title; }

