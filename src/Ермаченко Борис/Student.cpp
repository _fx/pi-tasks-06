#include "Student.h"



Student::Student()
{
	fio = "";
	spec = 0;
	year = 1;
	number = 0;
	marks = 0;
	id = 0;
}

Student::Student(const Student & st)
{
	fio = st.fio;
	spec = st.spec;
	year = st.year;
	id = st.id;
	number = st.number;
	if (number > 0) {
		marks = new int[number];
		for (int i = 0; i < number; i++)
			marks[i] = st.marks[i];
	}
}
Student::Student(string Fio, int Spec, int Id)
{
	fio = Fio;
	marks = 0;
	number = 0;
	spec = Spec;
	id = Id;
	year = 1;
	gr = nullptr;

}
Student * Student::getStudent()
{
	return this;
}

void Student::setStudent(string Fio, int Spec, int Id)
{
	fio = Fio;
	marks = 0;
	number = 0;
	spec = Spec;
	id = Id;
	year = 1;
	gr = nullptr;

}

void Student::toGroup(Group *gr1)
{
	gr = gr1;
}

void Student::setId(int Id)
{
	id = Id;
}

int Student::getId()
{
	return id;
}

int Student::getSpec()
{
	return spec;
}

Group* Student::getGr()
{
	return gr;
}

int* Student::getMark()
{
	return marks;
}

int Student::getNumMark()
{
	return number;
}

string Student::getFio()
{
	return fio;
}

bool Student::changeYear()
{
	return year++;
}

void Student::addMark(int m)
{
	if (number == 0) {
		marks = new int[1];
		marks[number++] = m;
	}
	else
	{
		int *tmp = new int[number + 1];
		for (int i = 0; i < number; i++)
			tmp[i] = marks[i];

		marks = new int[number + 1];
		marks = tmp;
		marks[number] = m;
		number++;
	}

}

void Student::showFio()
{
	cout << fio;
}

void Student::showMark()
{
	for (int i = 0; i < number; i++)
		cout << marks[i] << '_';
	cout << endl;
}

double Student::getmidMark()
{
	int sum=0;
	for (int i = 0; i < 10; i++)
		sum = sum + marks[i];
	double ret = (double)(sum / 10);
	return ret;
}

void Student::setGroup(Group * gr)
{
	this->gr = gr;
}


Student::~Student()
{
	delete[] marks;
}
