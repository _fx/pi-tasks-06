#include "Dekanat.h"

using namespace std;
Dekanat::Dekanat()
{
	numSt=0;
	numGr=0;
}

Dekanat::~Dekanat()
{
	int i = 0;
	for (i = 0; i < numSt; i++)
		delete st[i];
	for (i = 0; i < numGr; i++)
		delete gr[i];
	delete st;
	delete gr;
}

void Dekanat:: addSt(){
	int k = 0;
	string name,sekondnName,thirdName,Fio;
	int spec=0;
	int id=0;
	int grName = 0;
	ifstream in ("Student.txt",ios::binary);
	if (!in.is_open()) // ���� ���� �� ��� ������
		 {
		cout << "����  Student �� ����� ���� ������ \n";
		}
	while (in) {
		in >> name >> sekondnName >> thirdName >> spec >> grName >> id;
		Fio = name +" "+ sekondnName + " " + thirdName;
		if (numSt == 0) {
			k = 1;
			st = new Student*[numSt + 1];
			st[numSt] = new Student(Fio, spec, id);
		}
		else {
			Student **tmp = new Student*[numSt + 1];
			for (int i = 0; i < numSt; i++) {
				tmp[i] = st[i];
			}
			delete[] st;
			st = tmp;
			st[numSt] = new Student(Fio, spec, id);
		}
		for (int i = 0; i < numGr; i++) {
			if (gr[i]->getName() == grName) {
				gr[i]->addStudent(st[numSt]);
			}
		}

		numSt++;
	}
	in.close();

}

void Dekanat::addGr()
{
	int grName = 0;
	ifstream ing("Group.txt",ios::binary);
	if (!ing.is_open()) // ���� ���� �� ��� ������
	{
		cout << "����  Group �� ����� ���� ������ \n";
	}
	while (ing) {
		ing >> grName;
		if (numGr == 0) {
			gr = new Group*[numGr + 1];
			gr[numGr] = new Group(grName);

		}
		else {
			Group **tmp = new Group*[numGr + 1];
			for (int i = 0; i < numGr; i++) {
				tmp[i] = gr[i];
			}
			delete[] gr;
			gr = tmp;
			gr[numGr] = new Group(grName);
		}
			numGr++;	
	}
	ing.close();
}

void Dekanat::addMark()
{
	srand((int)time(0));
	random_device rd;
	for (int i = 0; i < numSt; i++) {
		//srand((int)time(NULL));
		int r = rd()%5 + 1;
		st[i]->addMark(r);
	}
}

int Dekanat::bestStudent()
{
	int maxi = 0;
	double maxm = 0;
	for (int i = 0; i < numSt; i++)
		if (st[i]->getmidMark() >= maxm) {
			maxi = i;
			maxm = st[i]->getmidMark();
		}
			return st[maxi]->getId();
		
}

Group * Dekanat::bestGroup()
{
	int maxi = 0;
	double maxm = 0;
	for(int i=0;i<numGr;i++)
		if (maxm <= gr[i]->midMark()) {
			maxi = i;
			maxm = gr[i]->midMark();
		}
	return gr[maxi];
}

Group * Dekanat::getGroup(int i)
{
	return gr[i];
}

int Dekanat::getnumSt()
{
	return numSt;
}

void Dekanat::moveStudent(int id, Group * gr)
{
	for (int i = 0; i < numSt; i++) 
		if (st[i]->getId() == id) {
			st[i]->getGr()->delStudent(id);
			st[i]->setGroup(gr);
		}

}

void Dekanat::delStudent()
{
	int ID = 0;	// ID �������� ��������
	double bad = 3;
	Student **tmp = nullptr;//��������� ������
	int count = numSt;		// ����� ���������
	for (int i = 0; i < numSt; i++)
		if (st[i]->getmidMark() < bad)
		{
			ID = st[i]->getId();
			st[i]->getGr()->delStudent(ID);
			delete st[i];
			st[i] = nullptr;
			count--;
		}

	tmp = new Student*[count];
	count = 0;
	for (int i = 0; i < numSt; i++)
		if (st[i])
		{
			tmp[count] = st[i];
			count++;
		}
	delete[] st;
	st = tmp;
	numSt = count;
}


void Dekanat::saveAll()
{

	ofstream sFile("Save.txt");
	if (!sFile.is_open())
	{
		cout << "oops, �������� � ������" << endl;
		exit(EXIT_FAILURE);
	}
	sFile << "UNIVERSITY STUDENTS:\n" << endl;
	for (int i = 0; i < numSt; i++) {
		int *tmpM;
		int num;
		tmpM = st[i]->getMark();
		num = st[i]->getNumMark();
		sFile << st[i]->getGr()->getName() << " " << st[i]->getFio() << "_" << st[i]->getId() << endl;
		for (int j = 0; j < num; j++)
		{
			sFile << tmpM[j];
		}
			if (i != numSt - 1)	sFile << endl;
	}
	sFile.close();
}
void Dekanat::showStudent(int id)
{
	for(int i=0;i<numSt;i++)
		if (st[i]->getId() == id) {
			st[i]->showFio();
			cout << endl;
			cout << "Spec:_" << st[i]->getSpec();
			cout << "_Marks=";
			st[i]->showMark();
			cout << "Id:_" << st[i]->getId();// << "_" << st[i]->getmidMark() << endl;
			break;
		}
}

void Dekanat::showGroup(int name)
{
	for (int i = 0; i < numGr; i++) {
		if (gr[i]->getName() == name) {
			cout << gr[i]->getName();
			gr[i]->showStudent();
			break;
		}
	}
}


void Dekanat::head()
{
	for(int i=0;i<numGr;i++)
	gr[i]->setHead();
}

