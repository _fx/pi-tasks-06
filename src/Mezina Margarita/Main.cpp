#define _CRT_SECURE_NO_WARNINGS
#include "Dekanat.h"
#include <vector>
#include <string>
#include <ctime>
using namespace std;
int main() {
	Dekanat dekanat;
	dekanat.LoadGroups();
	dekanat.LoadStud();
	dekanat.Scenario();
	dekanat.WriteStudents();
	dekanat.WriteGroups();
	return 0;
}